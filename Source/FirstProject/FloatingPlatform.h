// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FloatingPlatform.generated.h"

UCLASS()
class FIRSTPROJECT_API AFloatingPlatform : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFloatingPlatform();

	/** Mesh for the Platform */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Platform)
	class UStaticMeshComponent* Mesh;

	UPROPERTY(Editanywhere)
	FVector StartPoint;

	UPROPERTY(Editanywhere, meta = (MakeEditWidget = "true"))
	FVector EndPoint;

	UPROPERTY(Editanywhere, BlueprintReadOnly, Category = Platform)
	float InterpSpeed;

	UPROPERTY(Editanywhere, BlueprintReadOnly, Category = Platform)
	float InterpTime;

	FTimerHandle InterpTimer;

	UPROPERTY(Editanywhere, BlueprintReadOnly, Category = Platform)
	bool bInterping;

	float Distance;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void ToggleInterping();

	void SwapVectors(FVector& VecOne, FVector& VecTwo);

};
